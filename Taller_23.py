
print("Bienvenido. En este programa aprenderá como usar cadenas de texto, denominadas string. Se presentarán 8 ejemplos y ejercicios de cada cadena para entender su funcionalidad y componentes.")

#upper

print("Primer caso: Upper.  El método .upper() convetirá la cadena de texto ingresada en una cadena de texto toda en mayúscula")
print("Ejemplo con la palabra negocio")
cadena="negocio"
print(cadena.upper())
print("Para practicar lo aprendido, su turno:")
cadena2=input("Ingresar una cadena de texto:   ")
print("Con los datos ingresados, se acaba de imprimir una cadena de texto en minúscula.")
print(cadena2.upper())
print("Eso es todo para esta simulacion de la cadena de texto upper().")

#lower
print("Segundo caso: El método .lower() convetirá la cadena de texto ingresada en una cadena de texto toda en minúscula")
print("ejemplo con la cadena de texto EL CÓDIGO CADA VEZ SE VUELVE MÁS LARGO")
cadena3="EL CÓDIGO CADA VEZ SE VUELVE MÁS LARGO"
print(cadena3.lower())
print("Para practicar lo aprendido, su turno:")
cadena4=input("Ingresar una cadena de texto:   ")
print("Con los datos ingresados, se acaba de imprimir una cadena de texto en MAYÚSCULA.")
print(cadena4.upper())
print("Eso es todo para esta simulacion de la cadena de texto lower().")

#strip
print("Tercer caso: Strip es una cadena de texto usada simplemente para quitar espaciados que no se quieren ver impresos.")
print("Ejemplo con la cadena de texto     Bienvenidos    ")
cadena5="   Bienvenidos    "
print(cadena5.strip())
print("Para practicar lo aprendido, su turno:")
cadena6=input("Ingresar una cadena de texto=")
print("Para crear los espacios, usar la tecla Tab, ahora se imprimirá la cadena ingresada y en su contenido se usará cadena.strip()")
print(cadena6.strip())
print("Eso es todo para esta simulacion de la cadena de texto strip().")

#startswith
print("Cuarto caso: Startswith es una cadena de texto usada simplemente para comprobación de inicio de cadenas.")
print("Ejemplo: se usará la la cadena de texto la palabra tabulación")
cadena7="tabulacion"
print(cadena7.startswith("tab"))
print("Para practicar lo aprendido, su turno:")
cadena8=input("Ingresar una cadena de texto=")
print(cadena8.startswith(""))
print("Eso es todo para esta simulacion de la cadena de texto startwith().")

#endswith
print("Quinto caso: Endswith es una cadena de texto usada simplemente para comprobación de final de cadenas.")
print("Ejemplo: se usará la la cadena de texto la palabra tabulación")
cadena9="ion"
print(cadena9.endswith("ion"))
print("Para practicar lo aprendido, su turno:")
cadena10=input("Ingresar una cadena de texto=")
print(cadena10.endswith(""))
print("Eso es todo para esta simulacion de la cadena de texto endwith().")


#find
print("Sexto caso: Find es una cadena de texto usada simplemente para buscar un dato en la cadena.")
print("Ejemplo de cadena de texto ¿Pasaré calculo?")
cadena11="¿Pasaré calculo?"
print(cadena11.find("calculo"))
print("Para practicar lo aprendido, su turno:")
cadena12=input("Ingresar una cadena de texto=")
print(cadena12.find(""))
print("Eso es todo para esta simulacion de la cadena de texto find().")


#replace
print("Septimo caso: Replace es una cadena de texto usada simplemente para reemplazar un dato en la cadena.")
print("Ejemplo de cadena de texto Los últimos meses han estado muy divertidos")
cadena13= "Los últimos meses han estado muy divertidos"
print(cadena13.replace("divertidos", "aburridos"))
print("Para practicar lo aprendido, su turno:")
cadena14=input("Ingresar una cadena de texto=")
print(cadena14.replace(""))
print("Eso es todo para esta simulacion de la cadena de texto replace().")


#split 
print("Octavo caso: Split es una cadena de texto usada simplemente para romper una cadena.")
print("Ejemplo cadena de texto pasaré todas las materias ")
cadena15="pasaré todas las materias"
print(cadena15.split("  "))
print("Para practicar lo aprendido, su turno:")
cadena16=input("Ingresar una cadena de texto=")
print(cadena16.split(""))
print("Eso es todo para esta simulacion de la cadena de texto replace().")

